// Copyright Epic Games, Inc. All Rights Reserved.

#include "practical_work_2_1.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, practical_work_2_1, "practical_work_2_1" );

DEFINE_LOG_CATEGORY(Logpractical_work_2_1)
 