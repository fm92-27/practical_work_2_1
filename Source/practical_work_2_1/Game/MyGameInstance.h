// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "../FuncLibrary/Types.h"
#include "Engine/DataTable.h"
#include "../Weapons/WeaponDefault.h"
#include "MyGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class PRACTICAL_WORK_2_1_API UMyGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	//table
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSetting")
	UDataTable* WeaponInfo = nullptr;
	UFUNCTION(BlueprintCallable)
	bool GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo);
};
