// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "practical_work_2_1GameMode.generated.h"

UCLASS(minimalapi)
class Apractical_work_2_1GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	Apractical_work_2_1GameMode();
};



