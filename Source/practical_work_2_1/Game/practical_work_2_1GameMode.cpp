// Copyright Epic Games, Inc. All Rights Reserved.

#include "practical_work_2_1GameMode.h"
#include "practical_work_2_1PlayerController.h"
#include "../Character/practical_work_2_1Character.h"
#include "UObject/ConstructorHelpers.h"

Apractical_work_2_1GameMode::Apractical_work_2_1GameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = Apractical_work_2_1PlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Chracter/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}