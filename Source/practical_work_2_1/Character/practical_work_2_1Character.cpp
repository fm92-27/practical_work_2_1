// Copyright Epic Games, Inc. All Rights Reserved.

#include "practical_work_2_1Character.h"
#include "FatigueComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "../Game/MyGameInstance.h"

Apractical_work_2_1Character::Apractical_work_2_1Character()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	/*CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprint/Chracter/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;*/

	FatigueComponent = CreateDefaultSubobject<UFatigueComponent>(TEXT("FatigueComponent"));
}

void Apractical_work_2_1Character::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	/*if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}*/

	if (CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC)
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}

	ApplyForwardAcceleration();
	MovementTick(DeltaSeconds);
}

void Apractical_work_2_1Character::BeginPlay()
{
	Super::BeginPlay();

	InitWeapon(InitWeaponName);

	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}
}

void Apractical_work_2_1Character::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);
	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &Apractical_work_2_1Character::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &Apractical_work_2_1Character::InputAxisY);

	NewInputComponent->BindAction(TEXT("MovementModeChangeSprint"), IE_Pressed, this, &Apractical_work_2_1Character::PressedLeftShiftInput);
	NewInputComponent->BindAction(TEXT("MovementModeChangeSprint"), IE_Released, this, &Apractical_work_2_1Character::ReleasedLeftShiftInput);
	
	NewInputComponent->BindAction(TEXT("FireEvent"), IE_Pressed, this, &Apractical_work_2_1Character::InputAttackPressed);
	NewInputComponent->BindAction(TEXT("FireEvent"), IE_Released, this, &Apractical_work_2_1Character::InputAttackReleased);

	NewInputComponent->BindAction(TEXT("ReloadEvent"), IE_Released, this, &Apractical_work_2_1Character::TryReloadWeapon);
}

void Apractical_work_2_1Character::InputAxisX(float Value)
{
	AxisX = Value;
}

void Apractical_work_2_1Character::InputAxisY(float Value)
{
	AxisY = Value;
}

void Apractical_work_2_1Character::MovementTick(float DeltaTime)
{
	AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
	AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

	APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (myController)
	{
		FHitResult ResultHit;
		myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);

		float FindRotaterResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
		SetActorRotation(FQuat(FRotator(0.0f, FindRotaterResultYaw, 0.0f)));
		if (CurrentWeapon)
		{
			FVector Displacement = FVector(0);
			switch (MovementState)
			{
			case EMovementState::Aim_State:
				Displacement = FVector(0.0f, 0.0f, 160.0f);
				CurrentWeapon->ShouldReduceDispersion = true;
				break;
			case EMovementState::AimWalk_State:
				Displacement = FVector(0.0f, 0.0f, 160.0f);
				CurrentWeapon->ShouldReduceDispersion = true;
				break;
			case EMovementState::Walk_State:
				Displacement = FVector(0.0f, 0.0f, 120.0f);
				CurrentWeapon->ShouldReduceDispersion = false;
				break;
			case EMovementState::Run_State:
				Displacement = FVector(0.0f, 0.0f, 120.0f);
				CurrentWeapon->ShouldReduceDispersion = false;
				break;
			case EMovementState::SprintRun_State:
				break;
			default:
				break;
			}

			CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement;
		}
	}
	//
	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Green, FString::Printf(TEXT("MovementTick")));
	ChangeMovementState(); // <- job it!

	/*if (CurrentWeapon)
		if (FMath::IsNearlyZero(GetVelocity().Size(), 0.5f))
			CurrentWeapon->ShouldReduceDispersion = true;
		else
			CurrentWeapon->ShouldReduceDispersion = false;*/
}

void Apractical_work_2_1Character::CharacterUpdate()
{
	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, FString::Printf(TEXT("CharacterUpdate")));
	float ResSpeed = 600.0f;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementSpeedInfo.AimSpeedNormal;
		break;
	case EMovementState::AimWalk_State:
		ResSpeed = MovementSpeedInfo.AimSpeedWalk;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementSpeedInfo.WalkSpeedNormal;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementSpeedInfo.RunSpeedNormal;
		break;
	case EMovementState::SprintRun_State:
		ResSpeed = MovementSpeedInfo.SprintRunSpeedRun;
		break;
	default:
		break;
	}
	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
	//ChangeMovementState(); // <- don't job it
}

void Apractical_work_2_1Character::ChangeMovementState()
{
	//
	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, FString::Printf(TEXT("ChangeMovementState")));
	//
	// test switch
	//
	if (!WalkEnabled && !SprintRunEnabled && !AimEnabled)
	{
		MovementState = EMovementState::Run_State;
	}
	else
	{
		if (SprintRunEnabled)
		{
			WalkEnabled = false;
			AimEnabled = false;
			MovementState = EMovementState::SprintRun_State;
		}
		if (WalkEnabled && !SprintRunEnabled && AimEnabled)
		{
			MovementState = EMovementState::AimWalk_State;
		}
		else
		{
			if (WalkEnabled && !SprintRunEnabled && !AimEnabled)
			{
				MovementState = EMovementState::Walk_State;
			}
			else
			{
				if (!WalkEnabled && !SprintRunEnabled && AimEnabled)
				{
					MovementState = EMovementState::Aim_State;
				}
			}
		}
	}
	CharacterUpdate();

	//Weapon state update
	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
		myWeapon->UpdateStateWeapon(MovementState);
}

void Apractical_work_2_1Character::ApplyForwardAcceleration()
{
	FVector ForwardVector = GetActorForwardVector();
	FVector CharacterVelocity = GetCharacterMovement()->Velocity;
	ForwardVector.Normalize();
	CharacterVelocity.Normalize();

	float Angle = FMath::Acos(FVector::DotProduct(ForwardVector, CharacterVelocity));
	float AngleToDegraas = FMath::RadiansToDegrees(Angle);
	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, FString::Printf(TEXT("Acos: %f"), Angle));
	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Green, FString::Printf(TEXT("Acos: %f"), AngleToDegraas));
	if (AngleToDegraas > 15)
	{
		SprintRunEnabled = false;
		AimEnabled = false;
		MovementState = EMovementState::Run_State;
	}
	//else /*if (MovementState == EMovementState::SprintRun_State)*/
	//{
	//}
	CharacterUpdate();
}

void Apractical_work_2_1Character::PressedLeftShiftInput()
{
	if (!SprintRunEnabled)
	{
		bLeftShiftInputEnabled = true;
	}
	else if (SprintRunEnabled)
	{
		bLeftShiftInputEnabled = false;
	}
	FatigueComponent->FatigueLebel(TimeTired, TimeRested);
	ChangeMovementState();
}

void Apractical_work_2_1Character::ReleasedLeftShiftInput()
{
	if (SprintRunEnabled)
	{
		SprintRunEnabled = false;
		bLeftShiftInputEnabled = false;
	}
	ChangeMovementState();
}

void Apractical_work_2_1Character::InputAttackPressed()
{
	AttackCharEvent(true);
}

void Apractical_work_2_1Character::InputAttackReleased()
{
	AttackCharEvent(false);
}

void Apractical_work_2_1Character::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		//ToDo Check melee or range
		myWeapon->SetWeaponStateFire(bIsFiring);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::AttackCharEvent - CurrentWeapon -NULL"));
}

AWeaponDefault* Apractical_work_2_1Character::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void Apractical_work_2_1Character::InitWeapon(FName IdWeapon)
{
	UMyGameInstance* myGI = Cast<UMyGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(IdWeapon, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					CurrentWeapon = myWeapon;

					myWeapon->WeaponSetting = myWeaponInfo;
					myWeapon->WeaponAddInfo.Round = myWeaponInfo.MaxRound;
					// DeBug
					//myWeapon->ReloadTimer = myWeaponInfo.ReloadTime;
					myWeapon->ReloadTime = myWeaponInfo.ReloadTime;
					myWeapon->UpdateStateWeapon(MovementState);

					myWeapon->OnWeaponReloadStart.AddDynamic(this, &Apractical_work_2_1Character::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &Apractical_work_2_1Character::WeaponReloadEnd);
				}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("InitWeapon - Weapon not found in table"));
		}
	}
}

void Apractical_work_2_1Character::TryReloadWeapon()
{
	if (CurrentWeapon)
	{
		if(CurrentWeapon->GetWeaponRound() <= CurrentWeapon->WeaponSetting.MaxRound)
		{ 
			CurrentWeapon->InitReload();
		}
	}
}

UDecalComponent* Apractical_work_2_1Character::GetCursorToWorld()
{
	return CurrentCursor;
}

void Apractical_work_2_1Character::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}

void Apractical_work_2_1Character::WeaponReloadEnd()
{
	WeaponReloadEnd_BP();
}

void Apractical_work_2_1Character::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
}

void Apractical_work_2_1Character::WeaponReloadEnd_BP_Implementation()
{
}

