// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "practical_work_2_1Character.h"
#include "FatigueComponent.generated.h"

class Apractical_work_2_1Character;
class UInputComponent;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PRACTICAL_WORK_2_1_API UFatigueComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UFatigueComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
	void FatigueLebel(float Tired, float Rested);

	UPROPERTY()
	Apractical_work_2_1Character* CharacterOwner;

	UPROPERTY()
	//APlayerController* PlayerController;
	UInputComponent* InputComponent;

	FTimerHandle TimerRun;
	FTimerHandle TimerTired;

	void StartTimerRunning();
	void FinishTimerRunning();

	float TimeTired;
	float TimeRested;
};
