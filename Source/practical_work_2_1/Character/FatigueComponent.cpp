// Fill out your copyright notice in the Description page of Project Settings.


#include "FatigueComponent.h"
#include "practical_work_2_1Character.h"
#include "TimerManager.h"
#include "GameFramework/PlayerController.h"

// Sets default values for this component's properties
UFatigueComponent::UFatigueComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UFatigueComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UFatigueComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UFatigueComponent::FatigueLebel(float Tired, float Rested)
{
	CharacterOwner = Cast<Apractical_work_2_1Character>(GetOwner());
	TimeTired = Tired;
	TimeRested = Rested;
	if (CharacterOwner)
	{
		if (CharacterOwner->bLeftShiftInputEnabled)
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, FString::Printf(TEXT("The character will get tired after %f seconds"), TimeTired));
			GetWorld()->GetTimerManager().SetTimer(TimerRun, this, &UFatigueComponent::StartTimerRunning, TimeTired, false);
			CharacterOwner->SprintRunEnabled = true;
		}
		else
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("The character is tired"));
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, FString::Printf(TEXT("the character can run through: %f"), TimeRested));
		}
	}
}

void UFatigueComponent::StartTimerRunning()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("The character is tired"));
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, FString::Printf(TEXT("The character can run through: %f"), TimeRested));
	CharacterOwner->MovementState = EMovementState::Run_State;
	GetWorld()->GetTimerManager().SetTimer(TimerTired, this, &UFatigueComponent::FinishTimerRunning, TimeRested, false);
}

void UFatigueComponent::FinishTimerRunning()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("The character rested"));
}




