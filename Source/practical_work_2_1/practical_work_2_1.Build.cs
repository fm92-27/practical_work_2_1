// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class practical_work_2_1 : ModuleRules
{
	public practical_work_2_1(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule" });
    }
}
