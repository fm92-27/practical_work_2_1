// Fill out your copyright notice in the Description page of Project Settings.


#include "SleeveBulletsDefault.h"

// Sets default values
ASleeveBulletsDefault::ASleeveBulletsDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SleeveBullets = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SleeveBullets"));
	SleeveBullets->SetSimulatePhysics(true);
	//SleeveBullets->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	SleeveBullets->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void ASleeveBulletsDefault::BeginPlay()
{
	Super::BeginPlay();

	EnablePhysics();
	
}

// Called every frame
void ASleeveBulletsDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void ASleeveBulletsDefault::EnablePhysics()
{
	if (SleeveBullets)
	{
		SleeveBullets->SetSimulatePhysics(true);
		//SleeveBullets->SetNotifyRigidBodyCollision(true);
	}
}

void ASleeveBulletsDefault::ApplyImpulse(FVector Impulse)
{
	if (SleeveBullets)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Green, FString::Printf(TEXT("ShellMesh: %f"), &ShellMesh));
		SleeveBullets->AddImpulse(Impulse, NAME_None, true);
		GetWorld()->GetTimerManager().SetTimer(TimerHandle, this, &ASleeveBulletsDefault::DestroySleeveBullets, DestroyTime, false);
	}
}

void ASleeveBulletsDefault::DestroySleeveBullets()
{
	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Green, FString::Printf(TEXT("Destroy")));
	this->Destroy();
}

