// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault_Grenade.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();
}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplose(DeltaTime);
}

void AProjectileDefault_Grenade::TimerExplose(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplose > TimeToExplose)
		{
			Explose();
		}
		else
		{
			TimerToExplose += DeltaTime;
		}
	}
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit
	(
		UPrimitiveComponent* HitComp, 
		AActor* OtherActor, 
		UPrimitiveComponent* OtherComp, 
		FVector NormalImpulse, 
		const FHitResult& Hit
	)
{
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);

	//homework
	float Duration = 5.0f;

	float FullDamage = ProjectileSetting.ProjectileMaxRadiusDamage;
	float HalfDamage = FullDamage * 1.5f;
	float EndDamage = FullDamage * 2.0f;

	DrawDebugSphere(GetWorld(), Hit.ImpactPoint, FullDamage, 32, FColor::Red, false, Duration, 0, 2.0f);
	DrawDebugSphere(GetWorld(), Hit.ImpactPoint, HalfDamage, 32, FColor::Yellow, false, Duration, 0, 2.0f);
	DrawDebugSphere(GetWorld(), Hit.ImpactPoint, EndDamage, 32, FColor::Green, false, Duration, 0, 2.0f);
}

void AProjectileDefault_Grenade::ImpactProjectile()
{
	TimerEnabled = true;
}

void AProjectileDefault_Grenade::Explose()
{
	TimerEnabled = false;
	if (ProjectileSetting.ExploseFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(
			GetWorld(), 
			ProjectileSetting.ExploseFX, 
			GetActorLocation(),
			GetActorRotation()
		);
	}
	if (ProjectileSetting.ExploseSound)
	{
		UGameplayStatics::PlaySoundAtLocation(
			GetWorld(),
			ProjectileSetting.ExploseSound,
			GetActorLocation()
		);
	}

	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(
		GetWorld(),
		ProjectileSetting.ExploseMaxDamage,
		ProjectileSetting.ExploseMaxDamage*0.2f,
		GetActorLocation(),
		1000.0f,
		2000.0f,
		5,
		NULL, IgnoredActor, nullptr, nullptr
	);

	this->Destroy();
}
