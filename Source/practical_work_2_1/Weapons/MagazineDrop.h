// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MagazineDrop.generated.h"

UCLASS()
class PRACTICAL_WORK_2_1_API AMagazineDrop : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMagazineDrop();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Components")
	class UStaticMeshComponent* Magazine = nullptr;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void EnablePhysics();

	void DropMagazine();

	void DestroyMagazine();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropTimeMagazine")
	float DestroyTime = 5.0f;

	FTimerHandle TimerHandle;

};
