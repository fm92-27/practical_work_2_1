// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponDefault.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

// Sets default values
AWeaponDefault::AWeaponDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);

	// homework
	SleeveBulletsLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("SleeveBullets"));
	SleeveBulletsLocation->SetupAttachment(RootComponent);
	MagazineLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("Magazine"));
	MagazineLocation->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();

	WeaponInit();
	
}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispensionTick(DeltaTime);

}

void AWeaponDefault::FireTick(float DeltaTime)
{
	if (GetWeaponRound() > 0)
	{
		if (WeaponFiring)
			if (FireTimer < 0.f)
			{
				if (!WeaponReloading)
				{
					Fire();
				}
			
			}
			
			else
				FireTimer -= DeltaTime;
	}
	else
		{
		if (!WeaponReloading)
		{
			InitReload();
			InitMagazine(GetProjectile());
		}
	}
}

void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if (WeaponReloading)
	{
		if(ReloadTimer < 0.0f)
		{
			FinishReload();
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::DispensionTick(float DeltaTime)
{
	if (!WeaponReloading)
	{
		if (!WeaponFiring)
		{
			if (ShouldReduceDispersion)
			{
				CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
			}
			else
			{
				CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
			}
		}
		if (CurrentDispersion < CurrentDispersionMin)
		{
			CurrentDispersion = CurrentDispersionMin;
		}
		else
		{
			if (CurrentDispersion > CurrentDispersionMax)
			{
				CurrentDispersion = CurrentDispersionMax;
			}
		}
	}
	UE_LOG(LogTemp,
		Warning,
		TEXT("Dispersion: MAX = %f, MIN = %f, Current = %f"),
		CurrentDispersionMax,
		CurrentDispersionMin,
		CurrentDispersion);
}

void AWeaponDefault::WeaponInit()
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}

	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent();
	}
}

void AWeaponDefault::SetWeaponStateFire(bool bIsFire)
{
	if (CheckWeaponCanFire())
		WeaponFiring = bIsFire;
	else
		WeaponFiring = false;
		FireTimer = 0.0f;
}

bool AWeaponDefault::CheckWeaponCanFire()
{
	return !BlockFire;
}

FProjectileInfo AWeaponDefault::GetProjectile()
{
	return WeaponSetting.ProjectileSetting;
}

void AWeaponDefault::Fire()
{
	FireTimer = WeaponSetting.RateOfFire;
	WeaponAddInfo.Round--;
	ChangeDispersionByShot();

	UGameplayStatics::SpawnSoundAtLocation(
		GetWorld(),
		WeaponSetting.SoundFireWeapon,
		ShootLocation->GetComponentLocation()
	);
	UGameplayStatics::SpawnEmitterAtLocation(
		GetWorld(),
		WeaponSetting.EffectFireWeapon,
		ShootLocation->GetComponentTransform()
	);

	int8 NumberProjectile = GetNumberProjectileByShot();


	if (ShootLocation)
	{
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();
		FProjectileInfo ProjectileInfo;
		ProjectileInfo = GetProjectile();
		FVector EndLocation;

		FHitResult HitResult;
		float Distance = FVector::Dist(SpawnLocation, HitResult.ImpactPoint);
		float BaseDamage = WeaponSetting.ProjectileSetting.ProjectileDamage;
		float DistanseMaxDamage = WeaponSetting.ProjectileSetting.ProjectileDistanseMaxDamage;
		float DistanceMinDamage = DistanseMaxDamage * 2.0f;
		float RadiusMaxDamage = WeaponSetting.ProjectileSetting.ProjectileMaxRadiusDamage;
		float RadiusMinDamage = RadiusMaxDamage * 2.0f;
		float Damage;

		for (int8 i = 0; i < NumberProjectile; i++)
		{
			EndLocation = GetFireEndLocation();
			AActor* HitActor = HitResult.GetActor();
			if (ProjectileInfo.Projectile)
			{
				InitSleeveBullets(ProjectileInfo);
				//FVector Dir = ShootEndLocation - SpawnLocation;
				FVector Dir = EndLocation - SpawnLocation;
				Dir.Normalize();

				FMatrix myMatrix(Dir, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
				SpawnRotation = myMatrix.Rotator();
				//Projectile Init ballistic fire

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AProjectileDefault* myProjectile = Cast<AProjectileDefault>
					(
						GetWorld()->SpawnActor
						(
							ProjectileInfo.Projectile,
							&SpawnLocation,
							&SpawnRotation, 
							SpawnParams
						)
					);
				if (myProjectile)
				{
					myProjectile->InitProjectile(WeaponSetting.ProjectileSetting);
					//ToDo Init Projectile settings by id in table row(or keep in weapon table)
					myProjectile->InitialLifeSpan = 20.0f;
					//Projectile->BulletProjectileMovement->InitialSpeed = 2500.0f;
				}
			}
			else
			{
				//ToDo Projectile null Init trace fire

				FCollisionQueryParams TraceParams;
				TraceParams.AddIgnoredActor(this);
				TraceParams.AddIgnoredActor(GetOwner());

				bool bHit = GetWorld()->LineTraceSingleByChannel(
					HitResult, SpawnLocation, EndLocation, ECC_Visibility, TraceParams
				);

				if (bHit)
				{
					DrawDebugLine(GetWorld(), SpawnLocation, HitResult.ImpactPoint, FColor::Red, false, 1.0f, 0, 2.0f);
				}
				else
				{
					DrawDebugLine(GetWorld(), SpawnLocation, EndLocation, FColor::Blue, false, 1.0f, 0, 2.0f);
				}
			}
			
			float DamageFromDistanse = GetDistanseDamage(Distance, BaseDamage, DistanseMaxDamage, DistanceMinDamage);
			float DamageFromRadius = GetEpicenterDamage(Distance, BaseDamage, RadiusMaxDamage, RadiusMinDamage);
			Damage = DamageFromDistanse * DamageFromRadius / BaseDamage;

			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, FString::Printf(TEXT("Damage: %f"), Damage));
			UGameplayStatics::ApplyDamage(
				HitActor,
				Damage,
				GetInstigatorController(),
				this,
				nullptr
			);
		}
	}
}

void AWeaponDefault::UpdateStateWeapon(EMovementState NewMovementState)
{
	//ToDo Dispersion
	BlockFire = false;

	switch (NewMovementState)
	{
	case EMovementState::Aim_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Aim_StateDispersionAinMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMix;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimRecroil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
	case EMovementState::AimWalk_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMix;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMix;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMix;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMix;
		break;
	case EMovementState::Walk_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Walk_StateDispersionAinMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMix;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMix;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Walk_StateDispersionReduction;
		break;
	case EMovementState::Run_State:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Run_StateDispersionAinMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMix;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Run_StateDispersionAimRecroil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Run_StateDispersionReduction;
		break;
	case EMovementState::SprintRun_State:
		BlockFire = true;
		SetWeaponStateFire(false);
		break;
	default:
		break;
	}

	ChangeDispersionByShot();
}

void AWeaponDefault::ChangeDispersionByShot()
{
	CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
}

float AWeaponDefault::GetCurrentDispersion() const
{
	float Result = CurrentDispersion;
	return Result;
}

FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.f);
}

FVector AWeaponDefault::GetFireEndLocation() const
{
	bool bShootDirection = false;
	FVector EndLocation = FVector(0);

	FVector tmpV = (ShootLocation->GetComponentLocation() - ShootEndLocation);

	if (tmpV.Size() > SizeVectorToShootDirectionLogic)
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(
			(ShootLocation->GetComponentLocation() - ShootEndLocation).GetSafeNormal()
		) * -20000.0f;
	}
	else
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(
			ShootLocation->GetForwardVector()
		) * 20000.0f;
	}

	
	return EndLocation;
}

int8 AWeaponDefault::GetNumberProjectileByShot() const
{
	return WeaponSetting.NumberProjectileByShot;
}

int32 AWeaponDefault::GetWeaponRound()
{
	return WeaponAddInfo.Round;
}

void AWeaponDefault::InitReload()
{
	WeaponReloading = true;
	ReloadTimer = WeaponSetting.ReloadTime;

	if(WeaponSetting.AnimCharReload)
		OnWeaponReloadStart.Broadcast(WeaponSetting.AnimCharReload);
}

void AWeaponDefault::FinishReload()
{
	WeaponReloading = false;
	WeaponAddInfo.Round = WeaponSetting.MaxRound;

	OnWeaponReloadEnd.Broadcast();
}


//homework
void AWeaponDefault::InitSleeveBullets(FProjectileInfo ProjectileInfo)
{
	if (WeaponSetting.SleeveBullets)
	{
		FVector SpawnLocation = SleeveBulletsLocation->GetComponentLocation();
		FRotator SpawnRotation = SleeveBulletsLocation->GetComponentRotation();
		ASleeveBulletsDefault* SleeveBullets = Cast<ASleeveBulletsDefault>
			(
				GetWorld()->SpawnActor
				(
					ProjectileInfo.SleeveBulletsClass,
					&SpawnLocation,
					&SpawnRotation
				)
			);
		if (SleeveBullets)
		{
			FVector Impulse = SpawnRotation.Vector() * 0.2f;
			SleeveBullets->ApplyImpulse(Impulse);
		}
	}
}

void AWeaponDefault::InitMagazine(FProjectileInfo ProjectileInfo)
{
	if (WeaponSetting.MagazineDrop)
	{
		FVector SpawnLocation = MagazineLocation->GetComponentLocation();
		FRotator SpawnRotation = MagazineLocation->GetComponentRotation();
		AMagazineDrop* Magazine = Cast<AMagazineDrop>
			(
				GetWorld()->SpawnActor
				(
					ProjectileInfo.MagazineClass,
					&SpawnLocation,
					&SpawnRotation
				)
			);
		if (Magazine)
		{
			Magazine->DropMagazine();
		}
	}
}

float AWeaponDefault::GetDistanseDamage(float Distance, float BaseDamage, float DistanseMaxDamage, float DistanceMinDamage)
{
	float Damage = BaseDamage;
	if (Distance > DistanseMaxDamage)
	{
		if (Distance >= DistanceMinDamage)
		{
			Damage = BaseDamage * (1.0f - WeaponSetting.ProjectileSetting.ExploseMaxDamage);
		}
		else
		{
			float Alpha = (Distance - DistanseMaxDamage) / (DistanceMinDamage - DistanseMaxDamage);
			float MinDamage = BaseDamage * (1.0f - WeaponSetting.ProjectileSetting.ExploseMaxDamage);
			Damage = FMath::Lerp(BaseDamage, MinDamage, Alpha);
		}
	}
	return Damage;
}

float AWeaponDefault::GetEpicenterDamage(float Distance, float BaseDamage, float RadiusMaxDamage, float RadiusMinDamage)
{
	float Damage = BaseDamage;

	if (Distance > RadiusMaxDamage)
	{
		if (Distance >= RadiusMinDamage)
		{
			Damage = BaseDamage * (1.0f - WeaponSetting.ProjectileSetting.ExploseMaxDamage);
		}
		else
		{
			float Alpha = (Distance - RadiusMaxDamage) / (RadiusMinDamage - RadiusMaxDamage);
			float MinDamage = BaseDamage * (1.0f - WeaponSetting.ProjectileSetting.ExploseMaxDamage);
			Damage = FMath::Lerp(BaseDamage, MinDamage, Alpha);
		}
	}
	return Damage;
}


