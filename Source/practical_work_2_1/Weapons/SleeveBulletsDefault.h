// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SleeveBulletsDefault.generated.h"

UCLASS()
class PRACTICAL_WORK_2_1_API ASleeveBulletsDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASleeveBulletsDefault();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Components")
	class UStaticMeshComponent* SleeveBullets = nullptr;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void EnablePhysics();

	void ApplyImpulse(FVector Impulse);

	void DestroySleeveBullets();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DestroySleeveBullets")
	float DestroyTime = 5.0f;

	FTimerHandle TimerHandle;

};
