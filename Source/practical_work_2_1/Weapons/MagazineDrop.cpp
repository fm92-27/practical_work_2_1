// Fill out your copyright notice in the Description page of Project Settings.


#include "MagazineDrop.h"

// Sets default values
AMagazineDrop::AMagazineDrop()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Magazine = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Magazine"));
	Magazine->SetSimulatePhysics(true);
	Magazine->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void AMagazineDrop::BeginPlay()
{
	Super::BeginPlay();

	EnablePhysics();

}

// Called every frame
void AMagazineDrop::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMagazineDrop::EnablePhysics()
{
	if (Magazine)
	{
		Magazine->SetSimulatePhysics(true);
	}
}

void AMagazineDrop::DropMagazine()
{
	if (Magazine)
	{
		FVector Drop = this->GetActorLocation().UpVector * 0.2f;
		Magazine->AddImpulse(Drop, NAME_None, true);
		GetWorld()->GetTimerManager().SetTimer(TimerHandle, this, &AMagazineDrop::DestroyMagazine, DestroyTime, false);
	}
}

void AMagazineDrop::DestroyMagazine()
{
	this->Destroy();
}

